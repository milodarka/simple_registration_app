startup:
	docker-compose up -d

start:
	@echo "-------------> start building application <-------------"
	if ! [ -f .env ];then cp .env.example .env;fi
	docker-compose up -d
	docker exec -it simpleApp_php /bin/sh -c "composer install;\
	php artisan migrate;"
	@echo "-------------> application succesfully started <--------------"
kill:
	@echo "-------------> start deleting application <------------------"
	docker-compose -f docker-compose.yml down
	docker image prune --all --force
	docker system prune --all --force
	@echo "-------------> application succesfully deleted on port 8000 <---------------"

