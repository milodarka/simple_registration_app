<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    use HasFactory;

    protected $table_name = 'users';

    protected $fillable = [
        'name',
        'lastname',
        'email',
        'password',
        'active'
    ];

    public static function getById($user_id)
    {
        $user = self::where('id', $user_id)
            ->first();

        return $user;
    }
    public static function getLastRecordedId()
    {
        $user_id = DB::table('users')->latest('id')->first();
        return $user_id->id;
    }
}