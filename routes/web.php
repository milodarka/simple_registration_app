<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/test',function(){

// });


Route::get('/', [LoginController::class, 'index']);
Route::get('/login', [LoginController::class, 'login']);
Route::post('/login-check', [LoginController::class, 'login_check'])->name('login-check');
Route::get('/register', [LoginController::class, 'register']);
Route::post('/address_type',  [UserController::class, 'address_type'])->name('add-address-type');
Route::get('/logout', [LogoutController::class, 'logout'])->name('logout');

/**
 * User info
 */
Route::prefix('user')->group(function () {
    Route::post('/register_user',  [UserController::class, 'registration'])->name('user-registration');
    Route::post('/edit_user',  [UserController::class, 'edit_user'])->name('user-edit');
    Route::post('/address_edit',  [UserController::class, 'address_edit'])->name('address-edit');
    Route::get('/user_home/{user_id}', [UserController::class, 'index']);
});