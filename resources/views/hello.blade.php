
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
</head>
<body>

<div class="bgimg-1">
  <div class="caption">
    <button class="btn-lg"> <a href="login">Log In</a></button>
    <button class="btn-lg"> <a href="register">Register</a></button>
  </div>
</div>
</body>
</html>